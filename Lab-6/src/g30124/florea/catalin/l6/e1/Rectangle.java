package g30124.florea.catalin.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int width;

    public Rectangle(String id,Color color,boolean fill,int x, int y, int length, int width) {
        super(id,color,fill,x,y);
        this.length = length;
        this.width = width;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(super.getX(),super.getY(),length,width);
        if(super.getFILL()==true)
            g.fillRect(super.getX(),super.getY(),length,width);
    }
}
