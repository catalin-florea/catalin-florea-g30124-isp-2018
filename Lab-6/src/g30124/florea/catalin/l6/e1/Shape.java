package g30124.florea.catalin.l6.e1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x,y;
    private String id;
    private boolean fill;

    public Shape(Color color) {
        this.color = color;
    }

    public Shape(String id, Color color,boolean fill, int x, int y) {
        this.id = id;
        this.color = color;
        this.fill=fill;
        this.x=x;
        this.y=y;
    }

    public String getID(){
        return this.id;
    }

    public boolean getFILL(){
        return this.fill;
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    public void setX(int x){
        this.x=x;
    }

    public void seY(int y){
        this.y=y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}
