package g30124.florea.catalin.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public class Circle implements Shape {
    private int radius;
    private Color color;
    private int x,y;
    private String id;
    private boolean fill;

    public Circle(Color color) {
        this.color = color;
    }

    public Circle(String id, Color color,boolean fill, int x, int y,int radius) {
        this.id = id;
        this.color = color;
        this.fill=fill;
        this.x=x;
        this.y=y;
        this.radius=radius;
    }

    public int getRadius() {
        return radius;
    }

    public String getID(){
        return this.id;
    }

    public boolean getFILL(){
        return this.fill;
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    public void setX(int x){
        this.x=x;
    }

    public void seY(int y){
        this.y=y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(color);
        g.drawOval(x,y,radius,radius);
        if(fill==true)
            g.fillOval(x,y,radius,radius);
    }
}
