package g30124.florea.catalin.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle implements Shape {
    private int length;
    private int width;
    private Color color;
    private int x,y;
    private String id;
    private boolean fill;

    public Rectangle(Color color) {
        this.color = color;
    }

    public Rectangle(String id, Color color,boolean fill, int x, int y, int length, int width) {
        this.id = id;
        this.color = color;
        this.fill=fill;
        this.x=x;
        this.y=y;
        this.length=length;
        this.width=width;
    }

    public String getID(){
        return this.id;
    }

    public boolean getFILL(){
        return this.fill;
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    public void setX(int x){
        this.x=x;
    }

    public void seY(int y){
        this.y=y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(color);
        g.drawRect(x,y,length,width);
        if(fill==true)
            g.fillRect(x,y,length,width);
    }

}

