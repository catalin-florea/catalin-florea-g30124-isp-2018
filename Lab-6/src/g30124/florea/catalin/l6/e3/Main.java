package g30124.florea.catalin.l6.e3;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = (Shape) new Circle("1",Color.RED,true,50,50,40);
        b1.addShape(s1);
        Shape s2 = new Circle("2",Color.GREEN,false,40,60,100);
        b1.addShape(s2);
        Shape s5 = new Circle("5",Color.GREEN,true,70,90,100);
        b1.addShape(s5);

        Shape s3 = (Shape) new Rectangle("3",Color.BLUE,false,0,480,40,20);
        b1.addShape(s3);

        Shape s4 = (Shape) new Rectangle("4",Color.BLACK,true,200,60,11,20);
        b1.addShape(s4);

        b1.deleteById("5");
        b1.deleteById("4");

    }
}
