package g30124.florea.catalin.l6.e3;

        import java.awt.Color;
        import java.awt.Graphics;

public interface Shape {

    public String getID();

    public abstract void draw(Graphics g);
}
