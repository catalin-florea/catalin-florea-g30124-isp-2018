package g30124.florea.catalin.l3.solution;

import java.util.Scanner;

public class Solution {


    public static int solution(int[] a) {
        int c;
        int i, j;
        for (i = 0; i < a.length; i++) {
            c = 1;
            for (j = 0; j < a.length; j++) {
                if ((i != j) && (a[j] == a[i]))
                    c = 0;
            }
            if (c == 1)
                return a[i];
        }
        return 0;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Numar elemente:");
        int n = in.nextInt();
        int i;
        int[] v = new int[30];

        for (i = 0; i < n; i++) {
            System.out.print("Element:");
            v[i] = in.nextInt();
        }
        System.out.print("Solutie: ");
        System.out.println(solution(v));
    }
}
