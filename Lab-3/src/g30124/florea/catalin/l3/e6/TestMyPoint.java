package g30124.florea.catalin.l3.e6;

import java.sql.SQLOutput;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(2,3);
        MyPoint p3 = new MyPoint(1,2);

        System.out.println(p1.toString()+"\n"+p2.toString());
        System.out.println(p2.distance(4,7));
        System.out.println(p2.distance2(p3));

        p3.setX(4);
        System.out.println("p3.x:"+p3.getX());

        p3.setXY(14,20);
        System.out.println(p3.toString());

        System.out.println("Distance to origin:"+p3.distance2(p1));
    }
}
