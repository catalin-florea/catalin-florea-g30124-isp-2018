package g30124.florea.catalin.l3.e3;

import becker.robots.*;

public class Movement {
    public static void main(String[] args) {
        // Set up the initial situation
        City ny = new City();
        Robot R2D2 = new Robot(ny, 1, 1, Direction.EAST);

        //Movement
        int i=0;
        for(i=0;i<5;i++) {
            R2D2.move();
        }

        //Turn
        R2D2.turnLeft();
        R2D2.turnLeft();

        //Return to starting point
        for(i=0;i<5;i++) {
            R2D2.move();
        }

    }
}
