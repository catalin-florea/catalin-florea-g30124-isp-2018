package g30124.florea.catalin.l3.e4;

import becker.robots.*;

public class WalkAround {
    public static void main(String[] args) {
        // Set up the initial situation
        City ny = new City();
        //Walls
        Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
        Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
        Wall blockAve2 = new Wall(ny, 2, 1, Direction.SOUTH);
        Wall blockAve3 = new Wall(ny, 2, 2, Direction.SOUTH);
        Wall blockAve4 = new Wall(ny, 2, 2, Direction.EAST);
        Wall blockAve5 = new Wall(ny, 1, 2, Direction.EAST);
        Wall blockAve6 = new Wall(ny, 1, 1, Direction.NORTH);
        Wall blockAve7 = new Wall(ny, 1, 2, Direction.NORTH);

        //Robot
        Robot R2D2 = new Robot(ny, 0, 2, Direction.WEST);

        //Movement
        R2D2.move();
        R2D2.move();
        R2D2.turnLeft();
        R2D2.move();
        R2D2.move();
        R2D2.move();
        R2D2.turnLeft();
        R2D2.move();
        R2D2.move();
        R2D2.move();
        R2D2.turnLeft();
        R2D2.move();
        R2D2.move();
        R2D2.move();
        R2D2.turnLeft();
        R2D2.move();

    }
}


