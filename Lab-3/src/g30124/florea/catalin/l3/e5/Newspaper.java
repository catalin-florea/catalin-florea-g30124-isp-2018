package g30124.florea.catalin.l3.e5;

import becker.robots.*;

public class Newspaper {
    public static void main(String[] args) {
        // Set up the initial situation
        City ny = new City();
        //Walls
        Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
        Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
        Wall blockAve2 = new Wall(ny, 2, 1, Direction.SOUTH);
        Wall blockAve3 = new Wall(ny, 1, 2, Direction.EAST);
        Wall blockAve4 = new Wall(ny, 1, 1, Direction.NORTH);
        Wall blockAve5 = new Wall(ny, 1, 2, Direction.NORTH);
        Wall blockAve6 = new Wall(ny, 1, 2, Direction.SOUTH);

        //Robot
        Robot R2D2 = new Robot(ny, 1, 2, Direction.SOUTH);

        //Newspaper
        Thing newspaper = new Thing(ny, 2, 2);

        //Movement
        R2D2.turnLeft();
        R2D2.turnLeft();
        R2D2.turnLeft();
        R2D2.move();
        R2D2.turnLeft();
        R2D2.move();
        R2D2.turnLeft();
        R2D2.move();
        R2D2.pickThing();
        R2D2.turnLeft();
        R2D2.turnLeft();
        R2D2.move();
        R2D2.turnLeft();
        R2D2.turnLeft();
        R2D2.turnLeft();
        R2D2.move();
        R2D2.turnLeft();
        R2D2.turnLeft();
        R2D2.turnLeft();
        R2D2.move();
        R2D2.turnLeft();
        R2D2.turnLeft();
        R2D2.turnLeft();
    }
}
