package g30124.florea.catalin.l8.e2;

import java.io.*;

public class Reader {
    public static void main(String[] args) throws IOException {
        //System.out.println("Searched character: ");
        char c=(char) System.in.read();
        File file=new File("date.txt");
        BufferedReader bf=new BufferedReader(new FileReader(file));
        String line;
        int counter=0;
        while((line=bf.readLine())!=null)
        {
            int line_counter=line.length();
            for(int i=0;i<line_counter;i++)
                if(line.charAt(i)==c)
                    counter++;
        }
        System.out.println("Count: " + counter);

    }
}