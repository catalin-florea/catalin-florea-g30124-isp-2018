package g30124.florea.catalin.l8.e1;

class CoffeeMaker {
    int counter=0;
    Coffee makeCoffee() throws NumberException {
        System.out.println("Make a coffee");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Coffee coffee = new Coffee(t,c);
        counter++;
        if(counter>13)
            throw new NumberException(counter,"Too many coffees!");
        return coffee;
    }

}//.class
