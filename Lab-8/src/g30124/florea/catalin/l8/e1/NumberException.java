package g30124.florea.catalin.l8.e1;

public class NumberException extends Exception {
    int n;
    public  NumberException(int n, String msg){
        super(msg);
        this.n=n;
    }

    int getNumber(){
        return n;
    }
}
