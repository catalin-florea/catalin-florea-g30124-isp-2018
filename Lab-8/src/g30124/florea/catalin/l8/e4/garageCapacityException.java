package g30124.florea.catalin.l8.e4;

public class garageCapacityException extends Exception {
    int nr;
    public garageCapacityException(String msj, int nr)
    {
        super(msj);
        this.nr=nr;
    }

    public int getNr() {
        return nr;
    }
}