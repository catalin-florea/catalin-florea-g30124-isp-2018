package g30124.florea.catalin.l8.e4;

import g30124.florea.catalin.l8.e1.NumberException;

import java.io.*;

public class Car_serializable {
    public static void main(String[] args) throws Exception{
        CarFactory factory = new CarFactory();

            Car car1 = factory.createCar("Audi A4", 45000);
            Car car2 = factory.createCar("Toyota Avensis", 33000);
            Car car3 = factory.createCar("Ford Mondeo", 40000);
            Car car4 = factory.createCar("Audi A8", 110000);
            Car car5 = factory.createCar("BMW i8", 130000);
            //Car car6 = factory.createCar("Ford Focus",19000);


            factory.send_to_garage(car1, "garage_1.dat");
            factory.send_to_garage(car2, "garage_2.dat");
            factory.send_to_garage(car3, "garage_3.dat");
            factory.send_to_garage(car4, "garage_4.dat");
            factory.send_to_garage(car5, "garage_5.dat");

            Car c4e = factory.extract_from_garage("garage_4.dat");

    }


    static class CarFactory{
        static int count=1,into_garage=0;

        Car createCar(String model, int price) throws numberException{
            if(count>5)
                throw new numberException("Too many cars in the factory: ",count);
            Car c = new Car(model,price);
            count++;
            System.out.println("Car " + model + " id:" + c.id +" was created");
            return c;
        }

        void send_to_garage(Car c,String garage_name) throws IOException,garageCapacityException{
            if(into_garage>5)
                throw new garageCapacityException("Garage full!",into_garage);
            into_garage++;

            ObjectOutputStream o =
                    new ObjectOutputStream(
                            new FileOutputStream(garage_name));

            o.writeObject(c);
            System.out.println(c+":into garage.");
        }

        Car extract_from_garage(String garage_name) throws IOException,ClassNotFoundException{
            ObjectInputStream in=new ObjectInputStream(new FileInputStream(garage_name));
            Car c=(Car)in.readObject();
            System.out.println(c+" on the road again.");
            into_garage--;
            return c;
        }
    }


    static class Car implements Serializable{
        String model;
        int price;
        transient int id;

        public Car(String model, int price) {
            this.model = model;
            this.price = price;
            id =  (int)(Math.random()*100);
        }


    }
}
