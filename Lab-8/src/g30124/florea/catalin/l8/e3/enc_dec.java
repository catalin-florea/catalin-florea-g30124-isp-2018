package g30124.florea.catalin.l8.e3;

import java.io.*;
import java.util.Scanner;

public class enc_dec {

    public static void Encription(String fileName) throws IOException{
        String line;

    //File file=new File("date.txt");
    BufferedReader bf=new BufferedReader(new FileReader(fileName));
    BufferedWriter wr=new BufferedWriter(new FileWriter("date.enc.txt"));

        while((line=bf.readLine())!=null){
        int counter=line.length();
        for(int i=0; i<counter;i++){
            char c=line.charAt(i);
            c++;
            wr.write(c);
        }
        wr.newLine();
    }
        wr.close();
}

    public static void Decription(String fileName) throws IOException{
        String line;

        //File file=new File("date.txt");
        BufferedReader bf=new BufferedReader(new FileReader(fileName));
        BufferedWriter wr=new BufferedWriter(new FileWriter("date.dec.txt"));

        while((line=bf.readLine())!=null){
            int counter=line.length();
            for(int i=0; i<counter;i++){
                char c=line.charAt(i);
                c--;
                wr.write(c);
            }
            wr.newLine();
        }
        wr.close();
    }

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);

        String file = "data.txt";
        String file_enc = "date.enc.txt";
        System.out.println("Enter your choice \n 1:Encription \n 2:Decription \n 3:Exit");
        int choice = in.nextInt();
        while (choice != 3) {
            if (choice == 1) {
                Encription(file);
                System.out.println("Files succesfully ecrypted");
            } else if (choice == 2) {
                Decription(file_enc);
                System.out.println("Files succesfully deecrypted");
            }
            System.out.println("Command:");
            choice = in.nextInt();


        }

    }
}
