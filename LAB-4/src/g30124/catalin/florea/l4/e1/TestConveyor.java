package g30124.catalin.florea.l4.e1;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestConveyor {

    private Conveyor c;

    @Before
    public void create(){
        c = new Conveyor();
    }

    @Test
    public void shouldAddBox(){
        Box b = new Box(c,0,1);
        assertEquals(b.getId(), c.getBox(0).getId());
    }

    @Test
    public void shouldMoveBoxToRight(){
        Box b = new Box(c,1,1);
        c.moveRight();
        assertEquals(b.getId(), c.getBox(2).getId());
    }

    @Test
    public void shouldMoveBoxToLeft(){
        Box b = new Box(c,1,1);
        c.moveLeft();
        assertEquals(b.getId(), c.getBox(0).getId());

    }

    @Test
    public void shouldpickBox(){
        Box b = new Box(c,1,1);
        assertEquals(c.pickBox(1).getId(), b.getId());
        assertEquals(null,c.getBox(1));
    }



}
