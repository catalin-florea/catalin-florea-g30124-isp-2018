package g30124.catalin.florea.l4.e4;

public class Test {

    public static void main(String[] args) {
        Author aut1 = new Author("Mircea Cartarescu", "mircea.cartarescu@hotmail.ro", 'm');
        Author aut2 = new Author("Carlos Ruiz Zafon", "zafon.official@icloud.com", 'm');

        Book book1 = new Book("Orbitor I",aut1,56,130);
        Book book2 = new Book("Umbra Vantului",aut2,35,120);

        System.out.println(book1.getName());
        System.out.println(book1.getAuthor());

        System.out.println(book2.toString());
        System.out.println(book2.getQtyInStock());

    }
}
