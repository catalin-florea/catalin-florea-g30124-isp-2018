package g30124.catalin.florea.l4.e3;

public class Test {

    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(2);
        Cylinder c3 = new Cylinder();
        Cylinder c4 = new Cylinder(2,4);

        System.out.println(c4.getVolume());
        System.out.println(c4.getArea());
    }
}
