package g30124.catalin.florea.l4.e3;

public class Cylinder extends Circle {
    double height;

    public Cylinder(){
        super();
        this.height=1.0;
    }


    public Cylinder(double radius){
        super(radius);
    }

    public Cylinder(double radius, double height){
        super(radius);
        this.height=height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return super.getArea()*height;
    }

    @Override
    public double getArea() {
        return 2*(super.getArea() + Math.PI* super.getRadius() * height);
    }
}
