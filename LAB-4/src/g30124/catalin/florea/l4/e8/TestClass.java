package g30124.catalin.florea.l4.e8;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

public class TestClass {
    private Shape shape1;
    private Circle circle1;
    private Rectangle rectangle1;
    private Square square1;

    @Before
    public void SetUp(){
        shape1 = new Shape("blue", true);
        //shape2 = new Shape("orange", false);
        circle1 = new Circle(3,"green",false);
        //circle2 = new Circle();
        rectangle1 = new Rectangle();
        //rectangle2 = new Rectangle(4,3);
        square1 = new Square();
        //square2 = new Square(2,"black",true);
    }

    @Test
        public void shouldSetShape() {
        shape1.setColor("white");
        assertEquals("white",shape1.getColor());
        shape1.setFilled(false);
        assertEquals(false,shape1.isFilled());
    }

    @Test
        public void shouldSetCircle() {
        circle1.setRadius(11.0);
        assertEquals(11.0, circle1.getRadius());
    }

    @Test
        public void shouldSetRectangle() {
        rectangle1.setLength(15.0);
        assertEquals(15.0, rectangle1.getLength());
        rectangle1.setWidth(16.0);
        assertEquals(16.0, rectangle1.getWidth());
    }

    @Test
        public void shouldSetSquare(){
        square1.setSide(13.0);
        assertEquals(13.0,square1.getSide());
    }

    @Test
    public void shouldGetPerimeter()
    {   assertEquals(4.0,rectangle1.getPerimeter());
        assertEquals(2*Math.PI*circle1.getRadius(),circle1.getPerimeter());
    }

    @Test
    public void shouldGetArea()
    {
        assertEquals(1.0,rectangle1.getArea());
        assertEquals(Math.PI*9,circle1.getArea());
    }

    @Test
    public void shouldPrint()
    {
        assertEquals("A Shape with color of blue and filled",shape1.toString());
        assertEquals("A Circle with radius=3.0 which is sublass of A Shape with color of green and Not filled",circle1.toString());
        assertEquals("A Square with side=1.0 ,which is a subclass of A Rectangle with width=1.0 and length=1.0 , which is a subclass of A Shape with color of green and filled",square1.toString());
    }

}
