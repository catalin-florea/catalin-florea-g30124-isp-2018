package g30124.catalin.florea.l4.e8;

public class Circle extends Shape {
    private double radius = 1.0;

    public Circle() {
        this.radius = 1.0;
    }

    public Circle(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return Math.PI*Math.pow(radius,2);
    }

    public double getPerimeter(){
        return 2*Math.PI*this.radius;
    }

    @Override
    public String toString() {
        return "A Circle with radius="+radius+" which is" +
                " sublass of "+ super.toString();
    }
}
