package g30124.catalin.florea.l4.e6;

import java.util.Arrays;

public class Book {
    private String name;
    private Author[] author = new Author[5];
    private double price;
    private int qtyInStock;

    public Book (String name, Author[] author, double price) {
        this.name=name;
        this.author=author;
        this.price=price;
    }

    public Book (String name, Author[] author, double price, int qtyInStock){
        this.name=name;
        this.author=author;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }

    public Author[] getAuthor() {
            return author;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return this.price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return "Book " +
                name + " by " +
                author.length + " authors";
    }

    public void printAuthors(){
        for(Author a: this.getAuthor())
            System.out.println(a);
    }
}

