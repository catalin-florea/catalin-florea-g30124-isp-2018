package g30124.catalin.florea.l4.e6;

public class TEst {
    public static void main(String[] args) {

        Author aut1 = new Author("Mircea Cartarescu", "mircea.cartarescu@hotmail.ro", 'm');
        Author aut2 = new Author("Carlos Ruiz Zafon", "zafon.official@icloud.com", 'm');
        Author[] authors = new Author[]{aut1,aut2};

        Book book1 = new Book("Orbitor I", authors, 56.0, 130);

        System.out.println(book1.getPrice());

        book1.printAuthors();
    }
}
