package g30124.catalin.florea.l4.e6;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestBook {

    private Author aut1,aut2;
    private Book book1;
    private Author[] authors;

    @Before
    public void SetUp() {
        aut1 = new Author("Mircea Cartarescu", "mircea.cartarescu@hotmail.ro", 'm');
        aut2 = new Author("Carlos Ruiz Zafon", "zafon.official@icloud.com", 'm');
        authors = new Author[]{aut1, aut2};

        book1 = new Book("Orbitor I", authors, 56.0, 130);
    }

    @Test
    public void shouldGetPrice()
    {
        assertEquals(56.0,book1.getPrice());
    }

    @Test
    public void shouldSetPrice(){
        book1.setPrice(50.0);
        assertEquals(50.0,book1.getPrice());
    }

    @Test
    public void shouldGetName()
    {
        assertEquals("Orbitor I",book1.getName());
    }

}
