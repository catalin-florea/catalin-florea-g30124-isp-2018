package g30124.catalin.florea.l4.e2;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestMyPoint {
    private MyPoint p;

    @Before
    public void create(){
        p = new MyPoint();
    }

    @Test
    public void shouldsetX(){
    p.setX(3);
    assertEquals(3,p.getX());
    }

    @Test
    public void shouldsetY(){
        p.setY(2);
        assertEquals(2,p.getY());
    }

    @Test
    public void shouldsetXY(){
        p.setXY(5,6);
        assertEquals(5,p.getX());
        assertEquals(6,p.getY());
    }
}
