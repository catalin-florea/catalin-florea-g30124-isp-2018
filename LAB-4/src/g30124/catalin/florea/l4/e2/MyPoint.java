package g30124.catalin.florea.l4.e2;

public class MyPoint {
    private int x, y;

    public MyPoint() {
        x = 0;
        y = 0;
    }

    public MyPoint(int a, int b) {
        x = a;
        y = b;
    }

    void setX(int a) {
        x = a;
    }

    void setY(int a) {
        y = a;
    }

    void setXY(int a, int b) {
        x = a;
        y = b;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String toString(){
        return "("+getX()+","+getY()+")";
    }

    public double distance(int a, int b){
        return Math.sqrt(Math.pow(a-x,2) + Math.pow(b-y,2));
    }

    public double distance2(MyPoint a){
        return Math.sqrt(Math.pow(a.getX()-x,2) + Math.pow(a.getY()-y,2));
    }
}

