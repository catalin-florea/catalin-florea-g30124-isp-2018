package g30124.florea.catalin.l5.e2;


public class TestClass {


    public static void main(String[] args) {
        ProxyImage proxy_img=new ProxyImage("photo_one.jpg",false);
        ProxyImage proxy_img2=new ProxyImage("second_photo.png",true);
        System.out.println("----");

        proxy_img.display();
        System.out.println("----");
        proxy_img2.display();
    }

}
