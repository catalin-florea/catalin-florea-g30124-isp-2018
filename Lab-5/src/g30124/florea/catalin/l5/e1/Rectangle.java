package g30124.florea.catalin.l5.e1;

class Rectangle extends Shape {
     protected double width;
     protected double length;

     public Rectangle(){ }

     public Rectangle(double length, double width){
        this.width=width;
        this.length=length;
    }

    public Rectangle(double length, double width, String color, boolean filled){
         super(color,filled);
        this.width=width;
        this.length=length;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea() {
        return this.length*this.width;
    }

    public double getPerimeter() {
        return 2*(this.length+this.width);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", length=" + length +
                ", color='" + color + '\'' +
                ", filled=" + filled +
                '}';
    }
}
