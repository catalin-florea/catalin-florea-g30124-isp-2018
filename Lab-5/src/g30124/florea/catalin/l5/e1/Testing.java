package g30124.florea.catalin.l5.e1;

public class Testing {

    public static void main(String[] args) {
        Circle c1 = new Circle();
        Rectangle r1 = new Rectangle();
        Square s1 = new Square();

        Shape[] s = new Shape[]{c1,r1,s1};

        c1.setRadius(4);

        r1.setLength(2);
        r1.setWidth(3);

        s1.setSide(5);

        System.out.println();

        System.out.println("Area of the circle:"+c1.getArea());
        System.out.println("Perimeter of the circle:"+c1.getPerimeter());
        System.out.println("---------");

        System.out.println("Area of the rectangle:"+r1.getArea());
        System.out.println("Perimeter of the rectangle:"+r1.getPerimeter());
        System.out.println("---------");

        System.out.println("Area of the square:"+s1.getArea());
        System.out.println("Perimeter of the square:"+s1.getPerimeter());
        System.out.println("---------");
        System.out.println("---------");

        for(int i=0;i<s.length;i++){
            System.out.println("Area:"+s[i].getArea());
            System.out.println("Perimeter:"+s[i].getPerimeter());
            System.out.println("---------");
        }
    }
}
