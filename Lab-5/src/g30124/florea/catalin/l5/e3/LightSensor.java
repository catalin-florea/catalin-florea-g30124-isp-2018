package g30124.florea.catalin.l5.e3;

import java.util.Random;

public class LightSensor extends Sensor {
    private int light_value;

    @Override
    public int readValue() {
        Random rand = new Random();
        light_value = rand.nextInt(100);
        return light_value;
    }
}
