package g30124.florea.catalin.l5.e3;

import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    TemperatureSensor temp_s01 = new TemperatureSensor();
    LightSensor light_s01 = new LightSensor();

    Timer timer=new Timer();
    private int secondsPassed=0;

    TimerTask task=new TimerTask() {
        public void run() {
            secondsPassed++;
            if(secondsPassed>=20)
                timer.cancel();
            System.out.println("Second:" + secondsPassed + "\n" +
                    "Temperature: " + temp_s01.readValue() + " Light: " + light_s01.readValue());

        }
    };
    public void Control() {

        timer.scheduleAtFixedRate(task, 1000, 1000);
        {

        }
    }

    public static void main(String[] args) {
        Controller controller=new Controller();
        controller.Control();
    }
}


