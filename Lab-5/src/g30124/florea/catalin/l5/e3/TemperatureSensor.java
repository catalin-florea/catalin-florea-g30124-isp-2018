package g30124.florea.catalin.l5.e3;

import java.util.Random;

public class TemperatureSensor extends Sensor{
    private int temp_value;

    @Override
    public int readValue() {
        Random rand = new Random();
        temp_value = rand.nextInt(200);
        return temp_value;
    }

}
