package g30124.florea.catalin.l9.e2;

import java.awt.FlowLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class ButtonAndText extends JFrame {
    int counter=0;
    JTextArea CounterB;
    JLabel app;
    JButton push;

    public void init() {

        this.setLayout(null);
        int width = 150;
        int height = 20;

        app = new JLabel("Counter ");
        app.setBounds(10, 50, width, height);

        CounterB = new JTextArea();
        CounterB.setBounds(10,70,width,height);

        push = new JButton("Press to increase");
        push.setBounds(5,150,width, height);

        push.addActionListener(new PressedButton());

        add(app);
        add(CounterB);
        add(push);

    }

    ButtonAndText() {

        setTitle("Button counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 250);
        setVisible(true);
    }

    class PressedButton implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            ButtonAndText.this.CounterB.setText("");
            counter++;
            String display=String.valueOf(counter);
            //ButtonAndText.this.CounterB.append("Count: "+counter);
            ButtonAndText.this.CounterB.append(display);
        }

    }


    public static void main(String[] args) {
        new ButtonAndText();
    }
}