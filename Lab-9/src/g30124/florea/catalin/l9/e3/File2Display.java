package g30124.florea.catalin.l9.e3;

import java.awt.FlowLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;

public class File2Display extends JFrame {
    JTextField file;
    JLabel name;
    JButton push;
    JTextArea tArea;

    public void init() {

        this.setLayout(null);
        int width = 150;
        int height = 20;

        name = new JLabel("File Name: ");
        name.setBounds(10, 50, width, height);

        file = new JTextField();
        file.setBounds(70,50,width, height);

        push = new JButton("Press to upload");
        push.setBounds(5,150,width, height);

        tArea = new JTextArea();
        tArea.setBounds(10,180,350,100);

        push.addActionListener(new PressedButton());

        add(name);
        add(file);
        add(push);
        add(tArea);

    }

    File2Display() {

        setTitle("Display file content");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 450);
        setVisible(true);
    }

    class PressedButton implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            String text = File2Display.this.file.getText();
            //File2Display.this.tArea.setText(file_name);
           // File file=new File(file_name);
            BufferedReader bf=null;
            try {
            bf=new BufferedReader(new FileReader(text));
                System.out.println(bf.lines());
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            try {
                text=bf.readLine();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            while(text!=null)
            {
                tArea.append(text);
                tArea.append("\n");
                try {
                    text=bf.readLine();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }


        }

    }

    public static void main(String[] args) {
        new File2Display();
    }
}
