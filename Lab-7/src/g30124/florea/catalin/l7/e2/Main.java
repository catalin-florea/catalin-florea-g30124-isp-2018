package g30124.florea.catalin.l7.e2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {

        Bank BT = new Bank();

        BT.addAccount("Alin Voica", 9200);
        BT.addAccount("Silviu Marcu", 1200);
        BT.addAccount("Stefan Stan", 3140);
        BT.addAccount("Alfredo Verdi", 10140);
        BT.addAccount("William Vinci", 2340);
        BT.printAccounts();

        System.out.println("--------");
        System.out.println((BT.getAccount("Alin Voica")).toString());

        System.out.println("--------");


        ArrayList<BankAccount> l = BT.getAllAccounts();
        Collections.sort(l, BankAccount.ALPHAcmp);

        System.out.println("--------");
        System.out.println("-------");


        for(BankAccount x:l)
            System.out.println(x.toString());

    }
}
