package g30124.florea.catalin.l7.e2;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Bank {
    private ArrayList<BankAccount> accounts = new ArrayList();

    public void addAccount(String owner, double balance){
        BankAccount acc = new BankAccount(owner,balance);
        accounts.add(acc);
    }

    public void printAccounts(){
        Collections.sort(accounts);
        System.out.println("Current BANK accounts:");
        System.out.println();
        for (BankAccount x : accounts) {
            System.out.println(x.toString());
        }
    }

    public void printAccounts(double minBalance, double maxBalance){
        for (BankAccount x : accounts) {
            if((x.getBalance()>=minBalance)&&(x.getBalance()<=maxBalance)) {
                System.out.println(x.toString());
            }
        }
    }

    public BankAccount getAccount(String owner) {
        for (BankAccount x : accounts) {
            if (x.getOwner().equalsIgnoreCase(owner))
                return x;
        }
        return null;
    }


    public ArrayList<BankAccount> getAllAccounts(){
        ArrayList<BankAccount> AllAccounts = new ArrayList();
        for(BankAccount x:accounts){
            AllAccounts.add(x);
        }
        return AllAccounts;
    }


}
