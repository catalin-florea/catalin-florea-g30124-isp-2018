package g30124.florea.catalin.l7.e2;

import java.util.Comparator;
import java.util.Objects;

public class BankAccount implements Comparable<BankAccount> {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }


    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void widraw(double amount){
        balance=balance-amount;
    }

    public void deposit(double amount){
        balance=balance+amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof g30124.florea.catalin.l7.e1.BankAccount)) return false;
        g30124.florea.catalin.l7.e1.BankAccount that = (g30124.florea.catalin.l7.e1.BankAccount) o;
        return Double.compare(this.balance, balance) == 0 &&
                Objects.equals(owner, this.owner);
    }

    @Override
    public int hashCode() {

        return Objects.hash(owner, balance);
    }

    @Override
    public int compareTo(BankAccount o) {
        if (this.getBalance() > o.getBalance())
            return 1;
        if (this.getBalance() == o.getBalance())
            return 0;
        if (this.getBalance() < o.getBalance())
            return -1;
        return 0;
    }

    @Override
    public String toString() {
        return "Owner:"+this.getOwner() + '\n'+
                "Balance"+this.getBalance() + '\n' +
                "----";
    }

    public static Comparator<BankAccount> ALPHAcmp = new Comparator<BankAccount>() {
        @Override
        public int compare(BankAccount o1, BankAccount o2) {
            String name1=o1.getOwner();
            String name2=o2.getOwner();
            return name1.compareTo(name2);
        }
    };

    public static void main(String[] args) {
        boolean TT;
        BankAccount acc1;
        acc1 = new BankAccount("Codrin", 1900.0);
        BankAccount acc2=new BankAccount("Stefan", 1000.0);
        BankAccount acc3=new BankAccount("Stefan", 1000.0);

        TT=acc2.equals(acc3);
        System.out.println(TT);
        System.out.println("----");

        System.out.println(acc1.getBalance());
        acc1.deposit(1200);
        System.out.println(acc1.getBalance());
        System.out.println("----");

        System.out.println(acc3.getBalance());
        acc3.widraw(400);
        System.out.println(acc3.getBalance());
        System.out.println("----");


    }
}



