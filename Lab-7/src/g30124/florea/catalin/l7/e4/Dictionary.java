package g30124.florea.catalin.l7.e4;

import java.util.HashMap;

public class Dictionary {
    HashMap<Word,Definition> dicto = new HashMap();

    public void addWord(Word w, Definition d){
        if(dicto.containsKey(w))
            System.out.println("World already exists.");
        else
            System.out.println("Adding word...");
        dicto.put(w,d);
    }

    public Definition getDefinition(Word w){
        if(dicto.containsKey(w))
        return (Definition) dicto.get(w);
        else
            System.out.println("Word does not exist in the dictionary.");
        return null;
    }

    public void getAllWords()
    {
        dicto.forEach((key,value)-> System.out.println(key));
    }
    public void getAllDefinitions()
    {
        dicto.forEach((key, value) -> System.out.println(value));
    }

    public void getDictionary(){
        dicto.forEach((key,value)->
        {
            System.out.println(key);
            System.out.println(value);
        });
    }
}
