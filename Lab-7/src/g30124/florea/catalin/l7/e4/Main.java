package g30124.florea.catalin.l7.e4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        Dictionary dictionary = new Dictionary();
        char choice;
        Word w;
        Definition d;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        String line;

        do {
            System.out.println("----- MENU -----");
            System.out.println("A: ADD WORD");
            System.out.println("S: SEARCH FOR WORD");
            System.out.println("D: SHOW ALL DEFINITIONS");
            System.out.println("W: SHOW ALL WORDS");
            System.out.println("O: SHOW ALL WORDS");
            System.out.println("E: EXIT.");

            line = fluxIn.readLine();
            choice = line.charAt(0);

            switch (choice) {
                case 'a': case 'A':
                    System.out.println("Add word:");
                    line = fluxIn.readLine();
                    if (line.length() > 1) {
                        w = new Word(line);
                        System.out.println("Insert definition:");
                        line = fluxIn.readLine();
                        d = new Definition(line);
                        dictionary.addWord(w, d);
                    }
                    break;
                case 's': case 'S':
                    System.out.println("Search word:");
                    line = fluxIn.readLine();
                    if (line.length() > 1) {
                        w = new Word(line);
                        Definition searched = dictionary.getDefinition(w);
                        if (searched == null)
                            System.out.println("Word NOT found.");
                        else
                            System.out.println(searched.toString());

                    }
                    break;
                case 'w': case 'W':
                    dictionary.getAllWords();
                    break;
                case 'd': case 'D':
                    dictionary.getAllDefinitions();
                    break;
                case 'o': case 'O':
                    dictionary.getDictionary();
                    break;
            }
        } while (choice != 'e' && choice != 'E');
    }
}
