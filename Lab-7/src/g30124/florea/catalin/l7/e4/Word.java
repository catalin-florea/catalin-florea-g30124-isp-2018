package g30124.florea.catalin.l7.e4;

import java.util.Objects;

public class Word {
    private String name;

    public Word(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return " Word: " + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Word)) return false;
        Word word = (Word) o;
        return Objects.equals(getName(), word.getName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName());
    }
}
