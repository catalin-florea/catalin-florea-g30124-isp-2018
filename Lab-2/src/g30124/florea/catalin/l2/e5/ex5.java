package g30124.florea.catalin.l2.e5;

import java.util.*;

public class ex5 {
    public static void main(String[] args){
        int i,j;
        int aux;

        Random r = new Random();

        int[] a = new int[10];

        for(i=0;i<a.length;i++){
            a[i] = r.nextInt(100);
        }

        System.out.println("Vector nesortat");
        for(i=0;i<a.length;i++){
            System.out.print("a["+i+"]="+a[i]+" ");
        }

        for(i=0;i<a.length-1;i++)
            for(j=i+1;j<a.length;j++)
                if(a[j]<a[i]){
                    aux=a[i];
                    a[i]=a[j];
                    a[j]=aux;
                }

        System.out.println("Vector sortat");
        for(i=0;i<a.length;i++){
            System.out.print("a["+i+"]="+a[i]+" ");
        }
    }
}
