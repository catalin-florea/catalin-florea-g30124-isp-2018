package g30124.florea.catalin.l2.e7;

import java.util.*;

public class ex7 {
    public static void main(String[] args){
        int tries=0;

        Random r = new Random();
        int j = r.nextInt(10);
        while(tries<3){
            Scanner in = new Scanner(System.in);
            int x = in.nextInt();
            if(x==j) System.out.println("Guessed");
            else tries++;
        }
        if(tries==3)
            System.out.println("You lost");
        System.out.println("Number was:");
        System.out.println(j);
    }
}
