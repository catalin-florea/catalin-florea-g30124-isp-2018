package g30124.florea.catalin.l2.e6;

import java.util.*;
public class ex6 {

    static int RecFactorial(int nr ){
        if (nr==1)
            return 1;
        else return nr*RecFactorial(nr-1);
    }


    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int fact1=1;
        int i;

        for(i=1;i<=n;i++)
            fact1=fact1*i;
        System.out.println(fact1);
        System.out.println(RecFactorial(n));


    }
}

